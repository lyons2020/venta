package lyons.ventas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lyons.ventas.models.ParametroEntity;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParametroDTO {
  private Integer id;
  @NotNull
  private Integer numPrefijo;
  @NotNull
  private Integer correlativo;
  private String descripcion;
  private String abreviatura;
  private Integer estado;
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaCreacion;
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaModificacion;

  public ParametroDTO(ParametroEntity parametroEntity) {
    this.id = parametroEntity.getId();
    this.numPrefijo = parametroEntity.getNumPrefijo();
    this.correlativo = parametroEntity.getCorrelativo();
    this.descripcion = parametroEntity.getDescripcion();
    this.abreviatura = parametroEntity.getAbreviatura();
    this.estado = parametroEntity.getEstado();
    this.fechaCreacion = parametroEntity.getFechaCreacion();
    this.fechaModificacion = parametroEntity.getFechaModificacion();
  }

  public ParametroEntity toEntity(){
    var entity = new ParametroEntity();
    entity.setId(this.id);
    entity.setNumPrefijo(this.numPrefijo);
    entity.setCorrelativo(this.correlativo);
    entity.setDescripcion(this.descripcion);
    entity.setAbreviatura(this.abreviatura);
    entity.setEstado(this.estado);
    entity.setFechaCreacion(this.fechaCreacion);
    entity.setFechaModificacion(this.fechaModificacion);
    return entity;
  }
}
