package lyons.ventas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lyons.ventas.models.FacturaEntity;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FacturaDTO {
  private Integer id;
  @NotNull
  private String numero;
  @NotNull
  private ClienteDTO cliente;
  @NotNull
  private LocalDateTime fecha;
  private Integer estado;
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaCreacion;
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaModificacion;

  public FacturaDTO(FacturaEntity facturaEntity) {
    this.id = facturaEntity.getId();
    this.numero = facturaEntity.getNumero();
    this.cliente = new ClienteDTO(facturaEntity.getCliente());
    this.fecha = facturaEntity.getFecha();
    this.estado = facturaEntity.getEstado();
    this.fechaCreacion = facturaEntity.getFechaCreacion();
    this.fechaModificacion = facturaEntity.getFechaModificacion();
  }

  public FacturaEntity toEntity() {
    var entity = new FacturaEntity();
    entity.setId(this.id);
    entity.setNumero(this.numero);
    entity.setCliente(this.cliente.toEntity());
    entity.setFecha(this.fecha);
    entity.setEstado(this.estado);
    entity.setFechaCreacion(this.fechaCreacion);
    entity.setFechaModificacion(this.fechaModificacion);
    return entity;
  }
}
