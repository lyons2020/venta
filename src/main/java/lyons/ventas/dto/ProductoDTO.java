package lyons.ventas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lyons.ventas.base.Constantes;
import lyons.ventas.models.ProductoEntity;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductoDTO {
  private Integer id;
  @NotNull(message = "Debe seleccionar una categoría")
  private Integer idCategoria;
  @NotNull(message = "Nombre es un campo obligatorio")
  private String nombre;
  @NotNull(message = "Precio es un campo obligatorio")
  private BigDecimal precio;
  private Integer stock;
  private Integer estado = Constantes.HABILITADO;
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaCreacion = LocalDateTime.now();
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaModificacion = LocalDateTime.now();

  private ParametroDTO categoria;

  public ProductoDTO(ProductoEntity productoEntity) {
    var categoria = productoEntity.getCategoria();
    this.id = productoEntity.getId();
    this.categoria = ParametroDTO.builder().id(categoria.getId()).descripcion(categoria.getDescripcion()).build();
    this.nombre = productoEntity.getNombre();
    this.precio = productoEntity.getPrecio();
    this.stock = productoEntity.getStock();
    this.estado = productoEntity.getEstado();
    this.fechaCreacion = productoEntity.getFechaCreacion();
    this.fechaModificacion = productoEntity.getFechaModificacion();
  }

  public ProductoEntity toEntity() {
    var entity = new ProductoEntity();
    var categoria = this.categoria.toEntity();
    entity.setId(this.id);
    entity.setCategoria(categoria);
    entity.setNombre(this.nombre);
    entity.setPrecio(this.precio);
    entity.setStock(this.stock);
    entity.setEstado(this.estado);
    entity.setFechaCreacion(this.fechaCreacion);
    entity.setFechaModificacion(this.fechaModificacion);
    return entity;
  }
}
