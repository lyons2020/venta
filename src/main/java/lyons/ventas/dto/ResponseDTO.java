package lyons.ventas.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO<T> {
  private int codigo;
  private String mensaje;
  private T data;

  public ResponseDTO(int codigo, T data) {
    this.codigo = codigo;
    this.data = data;
  }


}
