package lyons.ventas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lyons.ventas.models.DetalleEntity;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class DetalleDTO {
  private Integer id;
  @NotNull
  private FacturaDTO factura;
  @NotNull
  private ProductoDTO producto;
  @NotNull
  private Integer cantidad;
  @NotNull
  private BigDecimal precio;
  private Integer estado;
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaCreacion;
  @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
  private LocalDateTime fechaModificacion;

  public DetalleDTO(DetalleEntity detalleEntity) {
    this.id = detalleEntity.getId();
    this.factura = new FacturaDTO(detalleEntity.getFactura());
    this.producto = new ProductoDTO(detalleEntity.getProducto());
    this.cantidad = detalleEntity.getCantidad();
    this.precio = detalleEntity.getPrecio();
    this.estado = detalleEntity.getEstado();
    this.fechaCreacion = detalleEntity.getFechaCreacion();
    this.fechaModificacion = detalleEntity.getFechaModificacion();
  }

  public DetalleEntity toEntity() {
    var entity = new DetalleEntity();
    entity.setId(this.id);
    entity.setFactura(this.factura.toEntity());
    entity.setProducto(this.producto.toEntity());
    entity.setCantidad(this.cantidad);
    entity.setPrecio(this.precio);
    entity.setEstado(this.estado);
    entity.setFechaCreacion(this.fechaCreacion);
    entity.setFechaModificacion(this.fechaModificacion);
    return entity;
  }
}
