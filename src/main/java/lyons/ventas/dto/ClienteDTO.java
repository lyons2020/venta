package lyons.ventas.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lyons.ventas.base.Constantes;
import lyons.ventas.models.ClienteEntity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClienteDTO {
  private Integer id;
  @NotNull(message = "Tipo de persona es un campo obligatorio")
  private Integer idTipoPersona;
  @NotNull(message = "Tipo de documento es un campo obligatorio")
  private Integer idTipoDocumento;
  @NotNull(message = "Número de documento es un campo obligatorio")
  private String numDocumento;
  @NotNull(message = "Nombre de cliente es un campo obligatorio")
  private String nombre;
  @NotNull(message = "Apellidos de cliente es un campo obligatorio")
  private String apellido;
  @NotNull(message = "Dirección de cliente es un campo obligatorio")
  private String direccion;
  private String referencia;
  @Email
  private String correo;
  private String celular;
  private Integer estado = Constantes.HABILITADO;
  @JsonIgnore
  private LocalDateTime fechaCreacion = LocalDateTime.now();
  @JsonIgnore
  private LocalDateTime fechaModificacion = LocalDateTime.now();
  private ParametroDTO tipoPersona;
  private ParametroDTO tipoDocumento;

  public ClienteDTO(ClienteEntity entity) {
    var tipoPersona = entity.getTipoPersona();
    var tipoDocumento = entity.getTipoDocumento();
    this.id = entity.getId();
    this.tipoPersona = ParametroDTO.builder().id(tipoPersona.getId()).descripcion(tipoPersona.getDescripcion()).build();
    this.tipoDocumento = ParametroDTO.builder().id(tipoDocumento.getId()).descripcion(tipoDocumento.getDescripcion()).build();
    this.numDocumento = entity.getNumDocumento();
    this.nombre = entity.getNombre();
    this.apellido = entity.getApellido();
    this.direccion = entity.getDireccion();
    this.referencia = entity.getReferencia();
    this.correo = entity.getCorreo();
    this.celular = entity.getCelular();
    this.estado = entity.getEstado();
    this.fechaCreacion = entity.getFechaCreacion();
    this.fechaModificacion = entity.getFechaModificacion();
  }
  public ClienteEntity toEntity(){
    var entity = new ClienteEntity();
    var tipoPersona = this.tipoPersona.toEntity();
    var tipoDocumento = this.tipoDocumento.toEntity();
    entity.setId(this.id);
    entity.setTipoPersona(tipoPersona);
    entity.setTipoDocumento(tipoDocumento);
    entity.setNumDocumento(this.numDocumento);
    entity.setNombre(this.nombre);
    entity.setApellido(this.apellido);
    entity.setDireccion(this.direccion);
    entity.setReferencia(this.referencia);
    entity.setCorreo(this.correo);
    entity.setCelular(this.celular);
    entity.setEstado(this.estado);
    entity.setFechaCreacion(this.fechaCreacion);
    entity.setFechaModificacion(this.fechaModificacion);
    return entity;
  }
}
