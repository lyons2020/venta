package lyons.ventas.controller;

import lombok.extern.slf4j.Slf4j;
import lyons.ventas.dto.ClienteDTO;
import lyons.ventas.dto.ResponseDTO;
import lyons.ventas.exception.NotFoundException;
import lyons.ventas.repository.ClienteRepository;
import lyons.ventas.service.ClienteService;
import lyons.ventas.utils.ConstantesUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/clientes")
@Slf4j
public class ClienteController {
  private final ClienteService clienteService;
  private final ClienteRepository clienteRepository;

  public ClienteController(ClienteService clienteService, ClienteRepository clienteRepository) {
    this.clienteService = clienteService;
    this.clienteRepository = clienteRepository;
  }

  /*****CREAR*****/
  @PostMapping
  public ResponseEntity<ResponseDTO> guardar(@RequestBody ClienteDTO clienteDTO) {
    var result = clienteService.guardar(clienteDTO);
    var responseBody = new ResponseDTO<>(0, "Cliente registrado correctamente", result);
    return ResponseEntity.ok(responseBody);
  }

  /*****ACTUALIZAR*****/
  @PutMapping("/{id}")
  public ResponseEntity<ResponseDTO> actualizar(@PathVariable Integer id, @RequestBody ClienteDTO clienteDTO) {
    var result = clienteService.actualizar(id, clienteDTO);
    var responseBody = new ResponseDTO<>(0, "Cliente actualizado correctamente", result);
    return ResponseEntity.ok(responseBody);
  }

  /*****ELIMINAR*****/
  @DeleteMapping("/{id}")
  public ResponseEntity<ResponseDTO> eliminar(@PathVariable Integer id) {
    var result = clienteService.eliminar(id);
    var responseBody = new ResponseDTO<>(0, "Cliente eliminado correctamente", result);
    return ResponseEntity.ok(responseBody);
  }

  /*****ELIMINACIÓN LÓGICA*****/
  @PatchMapping("/{id}")
  public ResponseEntity<?> baja(@PathVariable Integer id) {
    var result = clienteService.baja(id);
    var responseBody = new ResponseDTO<>(0, "Cliente dado de baja", result);
    return ResponseEntity.ok(responseBody);
  }

  /*****OBTENER POR ID*****/
  @GetMapping("/{id}")
  public ResponseEntity<ResponseDTO> obtenerPorId(@PathVariable Integer id) {
    var result = clienteRepository.findById(id).orElseThrow(() -> new NotFoundException("para este cliente"));
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }

  /*****OBTENER TODOS*****/
  @GetMapping
  public ResponseEntity<ResponseDTO> obtenerTodos() {
    var result = clienteRepository.findAll();
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }

  /*****OBTENER LISTA PAGINADA*****/
  @GetMapping("/buscar")
  public ResponseEntity<ResponseDTO> buscarPorFiltro(@RequestParam(name = "apellido", required = false) String apellidos,
                                                     @RequestParam(name = "nombre", required = false) String nombre,
                                                     @RequestParam(defaultValue = "0") int page,
                                                     @RequestParam(defaultValue = ConstantesUtil.SIZE_PAGE) int size,
                                                     @RequestParam(defaultValue = "nombre") String order,
                                                     @RequestParam(defaultValue = "true") boolean asc
  ) {
    var result = clienteService.busqueda(apellidos, nombre, page, size, order,asc);
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }

  @GetMapping("/documento/{numero}")
  public ResponseEntity<ResponseDTO> obtenerPorNroDocumento(@PathVariable String numero) {
    var result = clienteService.busquedaDocumento(numero);
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }


}
