package lyons.ventas.controller;

import lombok.extern.slf4j.Slf4j;
import lyons.ventas.dto.ProductoDTO;
import lyons.ventas.dto.ResponseDTO;
import lyons.ventas.exception.NotFoundException;
import lyons.ventas.repository.ProductoRespository;
import lyons.ventas.service.ProductoService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/productos")
@Slf4j
public class ProductoController {
  private final ProductoService productoService;
  private final ProductoRespository productoRespository;

  public ProductoController(ProductoService productoService, ProductoRespository productoRespository) {
    this.productoService = productoService;
    this.productoRespository = productoRespository;
  }

  /*****CREAR*****/
  @PostMapping
  public ResponseEntity<ResponseDTO> guardar(@RequestBody ProductoDTO productoDTO) {
    var result = productoService.guardar(productoDTO);
    var responseBody = new ResponseDTO<>(0,"Producto registrado correctamente", result);
    return ResponseEntity.ok(responseBody);
  }

  /*****ACTUALIZAR*****/
  @PutMapping("/{id}")
  public ResponseEntity<ResponseDTO> actualizar(@PathVariable Integer id, @RequestBody ProductoDTO productoDTO) {
    var result = productoService.actualizar(id, productoDTO);
    var responseBody = new ResponseDTO<>(0,"Producto actualizado correctamente", result);
    return ResponseEntity.ok(responseBody);
  }

  /*****ELIMINAR*****/
  @DeleteMapping("/{id}")
  public ResponseEntity<ResponseDTO> eliminar(@PathVariable Integer id) {
    var result = productoService.eliminar(id);
    var responseBody = new ResponseDTO<>(0, "Producto eliminado correctamente",result);
    return ResponseEntity.ok(responseBody);
  }

  /*****ELIMINACIÓN LÓGICA*****/
  @PatchMapping("/{id}")
  public ResponseEntity<?> baja(@PathVariable Integer id) {
    var result = productoService.baja(id);
    var responseBody = new ResponseDTO<>(0, "Producto dado de baja",result);
    return ResponseEntity.ok(responseBody);
  }

  /*****OBTENER POR ID*****/
  @GetMapping("/{id}")
  public ResponseEntity<ResponseDTO> obtenerPorId(@PathVariable Integer id)
  {
    var result = productoRespository.findById(id).orElseThrow(() -> new NotFoundException("para este producto"));
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }

  /*****OBTENER TODOS*****/
  @GetMapping
  public ResponseEntity<ResponseDTO> obtenerTodos()
  {
    var result = productoRespository.findAll();
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }

  /*****OBTENER LISTA PAGINADA*****/
  @GetMapping("/buscar")
  public ResponseEntity<ResponseDTO> buscarPorFiltro( @RequestParam(name = "apellidos", required = false) String apellidos,
                                                      @RequestParam(name = "nombre", required = false) String nombre,
                                                      @PageableDefault(page=0, size=10, direction= Sort.Direction.DESC) Pageable pageable
  ) {
    var result = productoService.busqueda(nombre, pageable);
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }

  @GetMapping("/producto/{nombre}")
  public ResponseEntity<ResponseDTO> obtenerPorNombre(@PathVariable String nombre)
  {
    var result = productoService.busquedaNombre(nombre);
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }

}
