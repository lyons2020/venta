package lyons.ventas.controller;

import lombok.extern.slf4j.Slf4j;
import lyons.ventas.dto.ResponseDTO;
import lyons.ventas.service.MaestroService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/maestros")
@Slf4j
public class MaestroController {
  private final MaestroService maestroService;

  public MaestroController(MaestroService maestroService) {
    this.maestroService = maestroService;
  }

  @GetMapping
  public ResponseEntity<ResponseDTO<?>> getMaestros(@RequestParam(name = "prefijo") Integer prefijo) {
    var result = maestroService.busquedaPrefijo(prefijo);
    var responseBody = new ResponseDTO<>(0, null, result);
    return ResponseEntity.ok(responseBody);
  }
}
