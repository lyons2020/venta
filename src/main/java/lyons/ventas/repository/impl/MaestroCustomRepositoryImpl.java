package lyons.ventas.repository.impl;

import lyons.ventas.exception.NotFoundException;
import lyons.ventas.models.ParametroEntity;
import lyons.ventas.repository.MaestroCustomRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MaestroCustomRepositoryImpl implements MaestroCustomRepository {
  @PersistenceContext
  private EntityManager em;
  @Override
  public List<ParametroEntity> busquedaPrefijo(Integer prefijo) {
    var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ParametroEntity.class);
    var parametroEntityRoot = cq.from(ParametroEntity.class);
    var filtros = new ArrayList<Predicate>();
    if(prefijo != null) {
      filtros.add(cb.equal(parametroEntityRoot.get("numPrefijo"),  prefijo));
    }
    filtros.add(cb.greaterThan(parametroEntityRoot.get("correlativo"),  0));
    if(!filtros.isEmpty()) {
      var filtrosArray = filtros.toArray(new Predicate[0]);
      cq.where(filtrosArray);
    }

    return em.createQuery(cq).getResultList();
  }
}
