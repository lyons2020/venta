package lyons.ventas.repository.impl;

import lyons.ventas.exception.NotFoundException;
import lyons.ventas.models.ClienteEntity;
import lyons.ventas.repository.ClienteCustomRepository;
import lyons.ventas.utils.Paginador;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;

@Repository
public class ClienteCustomRepositoryImpl implements ClienteCustomRepository {
  @PersistenceContext
  private EntityManager em;

  @Override
  public Page<ClienteEntity> busquedaPageable(String apellidos,String nombre, Pageable pageable) {
    var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ClienteEntity.class);
    var clienteEntityRoot = cq.from(ClienteEntity.class);

    var filtros = filtros(cb,apellidos,nombre,clienteEntityRoot);

    if(!filtros.isEmpty()) {
      var filtrosArray = filtros.toArray(new Predicate[0]);
      cq.where(filtrosArray);
    }

    Long offset = pageable.getOffset();

    var result = em.createQuery(cq)
            .setMaxResults(pageable.getPageSize())
            .setFirstResult(offset.intValue())
            .getResultList();

    var paginador = new Paginador();

    return (Page<ClienteEntity>) paginador.paginate(result, pageable, ClienteEntity.class,em);
  }

  @Override
  public ClienteEntity busquedaDocumento(String nrodocumento) {
    var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ClienteEntity.class);
    var clienteEntityRoot = cq.from(ClienteEntity.class);
    var filtros = new ArrayList<Predicate>();
    if(nrodocumento != null) {
      filtros.add(cb.equal(clienteEntityRoot.get("numDocumento"),  nrodocumento));
    }

    if(!filtros.isEmpty()) {
      var filtrosArray = filtros.toArray(new Predicate[0]);
      cq.where(filtrosArray);
    }

    var r = em.createQuery(cq).getResultList();
    if (r.size()==0){
      throw new NotFoundException("para esta búsqueda");
    }

    return r.get(0);
  }

  private ArrayList<Predicate> filtros(CriteriaBuilder cb, String apellidos,String nombre, Root<ClienteEntity> clienteEntityRoot) {
    var filtros = new ArrayList<Predicate>();
    if(apellidos != null) {
      filtros.add(cb.like(clienteEntityRoot.get("apellido"),  "%" +  apellidos + "%"));
    }
    if(nombre != null) {
      filtros.add(cb.like(clienteEntityRoot.get("nombre"), "%" +  nombre + "%"));
    }
    return filtros;
  }


}
