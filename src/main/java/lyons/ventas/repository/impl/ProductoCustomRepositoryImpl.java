package lyons.ventas.repository.impl;

import lyons.ventas.exception.NotFoundException;
import lyons.ventas.models.ProductoEntity;
import lyons.ventas.repository.ProductoCustomRepository;
import lyons.ventas.utils.Paginador;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;

@Repository
public class ProductoCustomRepositoryImpl implements ProductoCustomRepository {
  @PersistenceContext
  private EntityManager em;
  @Override
  public Page<ProductoEntity> busquedaPageable(String nombre, Pageable pageable) {
    var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ProductoEntity.class);
    var productoEntityRoot = cq.from(ProductoEntity.class);

    var filtros = filtros(cb,nombre,productoEntityRoot);

    if(!filtros.isEmpty()) {
      var filtrosArray = filtros.toArray(new Predicate[0]);
      cq.where(filtrosArray);
    }

    Long offset = pageable.getOffset();

    var result = em.createQuery(cq)
            .setMaxResults(pageable.getPageSize())
            .setFirstResult(offset.intValue())
            .getResultList();

    var paginador = new Paginador();

    return (Page<ProductoEntity>) paginador.paginate(result, pageable, ProductoEntity.class,em);
  }

  @Override
  public ProductoEntity busquedaNombre(String nombre) {
    var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ProductoEntity.class);
    var productoEntityRoot = cq.from(ProductoEntity.class);
    var filtros = new ArrayList<Predicate>();
    if(nombre != null) {
      filtros.add(cb.equal(productoEntityRoot.get("nombre"),  nombre));
    }

    if(!filtros.isEmpty()) {
      var filtrosArray = filtros.toArray(new Predicate[0]);
      cq.where(filtrosArray);
    }

    var r = em.createQuery(cq).getResultList();
    if (r.size()==0){
      throw new NotFoundException("para esta búsqueda");
    }

    return r.get(0);
  }

  private ArrayList<Predicate> filtros(CriteriaBuilder cb, String nombre, Root<ProductoEntity> productoEntityRoot) {
    var filtros = new ArrayList<Predicate>();
    if(nombre != null) {
      filtros.add(cb.like(productoEntityRoot.get("nombre"),  "%" +  nombre + "%"));
    }
    return filtros;
  }
}
