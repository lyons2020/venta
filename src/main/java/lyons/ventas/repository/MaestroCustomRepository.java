package lyons.ventas.repository;

import lyons.ventas.models.ParametroEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface MaestroCustomRepository {
  List<ParametroEntity> busquedaPrefijo(Integer prefijo);
}
