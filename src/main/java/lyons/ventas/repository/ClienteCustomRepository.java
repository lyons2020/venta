package lyons.ventas.repository;

import lyons.ventas.models.ClienteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteCustomRepository {
  Page<ClienteEntity> busquedaPageable(String apellidos, String nombre, Pageable pageable);

  ClienteEntity busquedaDocumento(String nrodocumento);
}
