package lyons.ventas.repository;

import lyons.ventas.models.ProductoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoCustomRepository {
  Page<ProductoEntity> busquedaPageable(String nombre, Pageable pageable);
  ProductoEntity busquedaNombre(String nrodocumento);
}
