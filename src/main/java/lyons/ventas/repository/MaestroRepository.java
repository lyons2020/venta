package lyons.ventas.repository;

import lyons.ventas.models.ParametroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaestroRepository extends JpaRepository<ParametroEntity, Integer>, MaestroCustomRepository{
}
