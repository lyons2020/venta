package lyons.ventas.repository;

import lyons.ventas.models.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRespository extends JpaRepository<ProductoEntity, Integer>, ProductoCustomRepository {
}
