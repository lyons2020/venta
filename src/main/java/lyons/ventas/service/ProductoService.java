package lyons.ventas.service;

import lyons.ventas.dto.ProductoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public interface ProductoService {
  ProductoDTO guardar(ProductoDTO productoDTO);
  Page<ProductoDTO> busqueda(String nombre, Pageable pageable);
  ProductoDTO actualizar(Integer id, ProductoDTO productoDTO);
  Integer eliminar(Integer id);
  ProductoDTO busquedaNombre(String nombre);
  Integer baja(Integer id);
}
