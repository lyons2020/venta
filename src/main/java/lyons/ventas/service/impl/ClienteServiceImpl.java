package lyons.ventas.service.impl;

import lyons.ventas.base.Constantes;
import lyons.ventas.dto.ClienteDTO;
import lyons.ventas.dto.ParametroDTO;
import lyons.ventas.exception.NotFoundException;
import lyons.ventas.repository.ClienteRepository;
import lyons.ventas.service.ClienteService;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.stream.Collectors;


@Service
@Transactional(readOnly = true)
public class ClienteServiceImpl implements ClienteService {
  private final ClienteRepository clienteRepository;

  public ClienteServiceImpl(ClienteRepository clienteRepository) {
    this.clienteRepository = clienteRepository;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public ClienteDTO guardar(ClienteDTO clienteDTO) {
    clienteDTO.setTipoPersona(ParametroDTO.builder().id(clienteDTO.getIdTipoPersona()).build());
    clienteDTO.setTipoDocumento(ParametroDTO.builder().id(clienteDTO.getIdTipoDocumento()).build());
    var clienteEntity = clienteDTO.toEntity();
    var clienteNuevo = clienteRepository.save(clienteEntity);
    return new ClienteDTO(clienteNuevo);
  }

  @Override
  public Page<ClienteDTO> busqueda(String apellidos, String nombre, Integer page, Integer size, String order, boolean asc) {
    var pageable =
            !asc ? PageRequest.of(page, size, Sort.by(order).descending())
                    : PageRequest.of(page, size, Sort.by(order));
    var result = clienteRepository.busquedaPageable(apellidos, nombre, pageable);
    var clienteDTO = result.stream().map(ClienteDTO::new).collect(Collectors.toList());
    return new PageImpl<>(clienteDTO, pageable, result.getTotalElements());
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public ClienteDTO actualizar(Integer id, ClienteDTO clienteDTO) {
    var cliente = clienteRepository.findById(id).orElseThrow(() -> new NotFoundException("para este cliente"));
    clienteDTO.setId(id);
    clienteDTO.setTipoPersona(ParametroDTO.builder().id(clienteDTO.getIdTipoPersona()).build());
    clienteDTO.setTipoDocumento(ParametroDTO.builder().id(clienteDTO.getIdTipoDocumento()).build());
    clienteDTO.setFechaCreacion(cliente.getFechaCreacion());
    clienteDTO.setEstado(cliente.getEstado());
    var clienteEntity = clienteDTO.toEntity();
    var clienteNuevo = clienteRepository.save(clienteEntity);
    return new ClienteDTO(clienteNuevo);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Integer eliminar(Integer id) {
    var cliente = clienteRepository.findById(id).orElseThrow(() -> new NotFoundException("para este cliente"));
    clienteRepository.delete(cliente);
    return id;
  }

  @Override
  public ClienteDTO busquedaDocumento(String nrodocumento) {
    var result = clienteRepository.busquedaDocumento(nrodocumento);
    return new ClienteDTO(result);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Integer baja(Integer id) {
    var cliente = clienteRepository.findById(id).orElseThrow(() -> new NotFoundException("para este cliente"));
    if (cliente.getEstado() == Constantes.HABILITADO) {
      cliente.setEstado(Constantes.DESHABILITADO);
    } else {
      cliente.setEstado(Constantes.HABILITADO);
    }
    cliente.setFechaModificacion(LocalDateTime.now());
    clienteRepository.save(cliente);
    return id;
  }


}
