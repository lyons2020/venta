package lyons.ventas.service.impl;

import lyons.ventas.dto.ParametroDTO;
import lyons.ventas.repository.MaestroRepository;
import lyons.ventas.service.MaestroService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class MaestroServiceImpl implements MaestroService {
  private final MaestroRepository maestroRepository;

  public MaestroServiceImpl(MaestroRepository maestroRepository) {
    this.maestroRepository = maestroRepository;
  }

  @Override
  public List<ParametroDTO> busquedaPrefijo(Integer prefijo) {
    var result = maestroRepository.busquedaPrefijo(prefijo);
    var resultDTO = result.stream().map(ParametroDTO::new).collect(Collectors.toList());
    return resultDTO;
  }
}
