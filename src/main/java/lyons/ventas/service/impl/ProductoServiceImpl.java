package lyons.ventas.service.impl;

import lyons.ventas.base.Constantes;
import lyons.ventas.dto.ParametroDTO;
import lyons.ventas.dto.ProductoDTO;
import lyons.ventas.exception.NotFoundException;
import lyons.ventas.repository.ProductoRespository;
import lyons.ventas.service.ProductoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class ProductoServiceImpl implements ProductoService {
  private final ProductoRespository productoRespository;

  public ProductoServiceImpl(ProductoRespository productoRespository) {
    this.productoRespository = productoRespository;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public ProductoDTO guardar(ProductoDTO productoDTO) {
    productoDTO.setCategoria(ParametroDTO.builder().id(productoDTO.getIdCategoria()).build());
    var productoEntity = productoDTO.toEntity();
    var productoNuevo = productoRespository.save(productoEntity);
    return new ProductoDTO(productoNuevo);
  }

  @Override
  public Page<ProductoDTO> busqueda(String nombre, Pageable pageable) {
    var result = productoRespository.busquedaPageable(nombre, pageable);
    var productoDTO = result.stream().map(ProductoDTO::new).collect(Collectors.toList());
    return new PageImpl<>(productoDTO, pageable, result.getTotalElements());
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public ProductoDTO actualizar(Integer id, ProductoDTO productoDTO) {
    var producto = productoRespository.findById(id).orElseThrow(() -> new NotFoundException("para este producto"));
    productoDTO.setId(id);
    productoDTO.setCategoria(ParametroDTO.builder().id(productoDTO.getIdCategoria()).build());
    productoDTO.setFechaCreacion(producto.getFechaCreacion());
    productoDTO.setEstado(producto.getEstado());
    var productoEntity = productoDTO.toEntity();
    var productoNuevo = productoRespository.save(productoEntity);
    return new ProductoDTO(productoNuevo);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Integer eliminar(Integer id) {
    var producto = productoRespository.findById(id).orElseThrow(() -> new NotFoundException("para este producto"));
    productoRespository.delete(producto);
    return id;
  }

  @Override
  public ProductoDTO busquedaNombre(String nombre) {
    var result = productoRespository.busquedaNombre(nombre);
    return new ProductoDTO(result);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Integer baja(Integer id) {
    var producto = productoRespository.findById(id).orElseThrow(() -> new NotFoundException("para este producto"));
    if(producto.getEstado()==0){
      producto.setEstado(Constantes.DESHABILITADO);
    }else{
      producto.setEstado(Constantes.HABILITADO);
    }
    producto.setFechaModificacion(LocalDateTime.now());
    productoRespository.save(producto);
    return id;
  }
}
