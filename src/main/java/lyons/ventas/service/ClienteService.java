package lyons.ventas.service;

import lyons.ventas.dto.ClienteDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(readOnly = true)
public interface ClienteService {
  ClienteDTO guardar(ClienteDTO clienteDTO);
  Page<ClienteDTO> busqueda(String apellidos, String nombre, Integer page, Integer size, String order,boolean asc);
  ClienteDTO actualizar(Integer id, ClienteDTO clienteDTO);
  Integer eliminar(Integer id);
  ClienteDTO busquedaDocumento(String nrodocumento);
  Integer baja(Integer id);
}
