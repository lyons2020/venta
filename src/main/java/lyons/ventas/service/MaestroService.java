package lyons.ventas.service;


import lyons.ventas.dto.ParametroDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public interface MaestroService {
  List<ParametroDTO> busquedaPrefijo(Integer prefijo);
}
