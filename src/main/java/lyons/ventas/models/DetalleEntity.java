package lyons.ventas.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "detalle", schema = "lyons", catalog = "ventas_db")
@Entity
public class DetalleEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;
  @ManyToOne(optional = false)
  @JoinColumn(name = "factura_id",referencedColumnName = "id", nullable = false)
  private FacturaEntity factura;
  @ManyToOne(optional = false)
  @JoinColumn(name = "producto_id",referencedColumnName = "id", nullable = false)
  private ProductoEntity producto;
  @Basic
  @Column(name = "cantidad", nullable = false)
  private Integer cantidad;
  @Basic
  @Column(name = "precio", nullable = false, precision = 10, scale = 4)
  private BigDecimal precio;
  @Basic
  @Column(name = "estado", nullable = false)
  private Integer estado;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_creacion", nullable = false)
  private LocalDateTime fechaCreacion;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_modificacion")
  private LocalDateTime fechaModificacion;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    DetalleEntity that = (DetalleEntity) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}