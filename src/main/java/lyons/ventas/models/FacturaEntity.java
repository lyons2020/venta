package lyons.ventas.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "factura", indexes = {
        @Index(name = "idx_numero_factura", columnList = "numero", unique = true)
}, schema = "lyons", catalog = "ventas_db")
@Entity
public class FacturaEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;
  @Basic
  @Column(name = "numero", length = 10)
  private String numero;
  @ManyToOne(optional = false)
  @JoinColumn(name = "cliente_id",referencedColumnName = "id", nullable = false)
  private ClienteEntity cliente;
  @Basic
  @Column(name = "fecha")
  private LocalDateTime fecha;
  @Basic
  @Column(name = "estado", nullable = false)
  private Integer estado;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_creacion", nullable = false)
  private LocalDateTime fechaCreacion;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_modificacion")
  private LocalDateTime fechaModificacion;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    FacturaEntity that = (FacturaEntity) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}