package lyons.ventas.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "producto", indexes = {
        @Index(name = "idx_nombre_producto", columnList = "nombre", unique = true)
}, schema = "lyons", catalog = "ventas_db")
@Entity
public class ProductoEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;
  @ManyToOne(optional = false)
  @JoinColumn(name = "categoria",referencedColumnName = "id", nullable = false)
  private ParametroEntity categoria;
  @Basic
  @Column(name = "nombre", nullable = false, length = 50)
  private String nombre;
  @Basic
  @Column(name = "precio", precision = 10, scale = 4)
  private BigDecimal precio;
  @Basic
  @Column(name = "stock")
  private Integer stock;
  @Basic
  @Column(name = "estado", nullable = false)
  private Integer estado;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_creacion", nullable = false)
  private LocalDateTime fechaCreacion;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_modificacion")
  private LocalDateTime fechaModificacion;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    ProductoEntity that = (ProductoEntity) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}