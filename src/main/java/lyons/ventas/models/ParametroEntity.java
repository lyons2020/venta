package lyons.ventas.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "parametro", schema = "lyons", catalog = "ventas_db")
public class ParametroEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;
  @Basic
  @Column(name = "num_prefijo", nullable = false)
  private Integer numPrefijo;
  @Basic
  @Column(name = "correlativo", nullable = false)
  private Integer correlativo;
  @Basic
  @Column(name = "descripcion", length = 50)
  private String descripcion;
  @Basic
  @Column(name = "abreviatura", length = 10)
  private String abreviatura;
  @Basic
  @Column(name = "estado", nullable = false)
  private Integer estado;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_creacion", nullable = false)
  private LocalDateTime fechaCreacion;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_modificacion")
  private LocalDateTime fechaModificacion;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    ParametroEntity that = (ParametroEntity) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}