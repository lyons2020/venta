package lyons.ventas.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cliente", indexes = {
        @Index(name = "cliente_num_documento_key", columnList = "num_documento", unique = true),
        @Index(name = "idx_nombre_completo", columnList = "nombre, apellido", unique = true)
}, schema = "lyons", catalog = "ventas_db")
@Entity
public class ClienteEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;
  @ManyToOne
  @JoinColumn(name = "tipo_persona", referencedColumnName = "id", nullable = false)
  private ParametroEntity tipoPersona;
  @ManyToOne
  @JoinColumn(name = "tipo_documento", referencedColumnName = "id", nullable = false)
  private ParametroEntity tipoDocumento;
  @Basic
  @Column(name = "num_documento", nullable = false, length = 15, unique = true)
  private String numDocumento;
  @Basic
  @Column(name = "nombre", nullable = false, length = 50)
  private String nombre;
  @Basic
  @Column(name = "apellido", length = 50)
  private String apellido;
  @Basic
  @Column(name = "direccion", nullable = false, length = 50)
  private String direccion;
  @Basic
  @Column(name = "referencia", length = 50)
  private String referencia;
  @Basic
  @Column(name = "correo", length = 50)
  private String correo;
  @Basic
  @Column(name = "celular", length = 10)
  private String celular;
  @Basic
  @Column(name = "estado", nullable = false)
  private Integer estado;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_creacion")
  private LocalDateTime fechaCreacion;
  @JsonIgnore
  @Basic
  @Column(name = "fecha_modificacion")
  private LocalDateTime fechaModificacion;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    ClienteEntity that = (ClienteEntity) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}